DaysOfTheWeek = {
    SUNDAY: 0,
    MONDAY: 1,
    TUESDAY: 2,
    WEDNESDAY: 3,
    THURSDAY: 4,
    FRIDAY: 5,
    SATURDAY:  6,
}

MonthsOfTheYear = {
    JANUARY: 0,
    FEBRUARY: 1,
    MARCH: 2,
    APRIL: 3,
    MAY: 4,
    JUNE: 5,
    JULY:  6,
    AUGUST: 7,
    SEPTEMBER: 8,
    OCTOBER: 9,
    NOVEMBER: 10,
    DECEMBER: 11,
}

class HolidayRule {

    /**
     *
     * @param aDate
     * @returns {boolean}
     */
    IsHoliday(aDate){
        throw new Error('ERROR! Not implemented.');
    }
}

class DayOfTheWeekHolidayRule extends HolidayRule{
    constructor(aDayOfTheWeek) {
        super();
        this.dayOfTheWeek = aDayOfTheWeek;
    }

    IsHoliday(aDate){
        return this.dayOfTheWeek === aDate.getDay();
    }
}

class DayOfTheMonthHolidayRule extends HolidayRule {
    constructor(aMonth, aDayOfTheMonth) {
        super();
        this.month = aMonth;
        this.dayOfTheMonth = aDayOfTheMonth;
    }

    IsHoliday(aDate){
        return this.month === aDate.getMonth() &&
               this.dayOfTheMonth === aDate.getDate();
    }
}

class DateHolidayRule extends HolidayRule {
    constructor(aDate) {
        super();
        this.date = aDate;
    }

    IsHoliday(aDate){
        return this.date.getTime() === aDate.getTime();
    }
}

class CompoundRangeHolidayRule extends HolidayRule {
    constructor(from, to, holidayRule) {
        super();
        this.fromDate = from;
        this.toDate = to;
        this.holidayRule = holidayRule;
    }

    IsHoliday(aDate) {
        return aDate >= this.fromDate && aDate <= this.toDate && this.holidayRule.IsHoliday(aDate);
    }

}

class HolidayCalendar {

    /**
     * @type {Array<HolidayRule>}
     */
    _holidayRules = [];

    /**
     * Checks if a Date is a holiday
     * @param {Date} aDate
     * @returns {boolean}
     */
    IsHoliday(aDate) {
        return this._holidayRules.some((dayOfWeekHolidayRule) => dayOfWeekHolidayRule.IsHoliday(aDate));
    }

    /**
     *
     * @param {HolidayRule} aHolidayRule
     */
    AddHolidayRule(aHolidayRule) {
        this._holidayRules.push(aHolidayRule);
    }

}

describe('XYZ', function () {
    test('A day of the week can be a holiday', function(){
        //setup
        const holidayCalendar = new HolidayCalendar();
        //exercise
        holidayCalendar.AddHolidayRule(new DayOfTheWeekHolidayRule(DaysOfTheWeek.SATURDAY));
        const aSaturday = new Date(2014,2,1);
        //assertions
        expect(holidayCalendar.IsHoliday(aSaturday)).toBe(true);
    });

    test('A day of the week can be not a holiday', function(){
        const holidayCalendar = new HolidayCalendar();
        const aMonday = new Date(2014,2,3);
        expect(holidayCalendar.IsHoliday(aMonday)).toBe(false);
    });

    test('More than one day of the week can be a holiday', function(){
        const holidayCalendar = new HolidayCalendar();
        holidayCalendar.AddHolidayRule(new DayOfTheWeekHolidayRule(DaysOfTheWeek.SATURDAY));
        holidayCalendar.AddHolidayRule(new DayOfTheWeekHolidayRule(DaysOfTheWeek.SUNDAY));
        const aSunday = new Date(2014,2,2);
        const aSaturday = new Date(2014,2,1);
        expect(holidayCalendar.IsHoliday(aSunday)).toBe(true);
        expect(holidayCalendar.IsHoliday(aSaturday)).toBe(true);
    });

    test('5',function(){
        const holidayCalendar = new HolidayCalendar();
        holidayCalendar.AddHolidayRule(new DayOfTheMonthHolidayRule(MonthsOfTheYear.JANUARY,1));
        const aJanuaryFirst = new Date(2014,MonthsOfTheYear.JANUARY,1);
        expect(holidayCalendar.IsHoliday(aJanuaryFirst)).toBe(true);
    });

    test('6',function(){
        const holidayCalendar = new HolidayCalendar();
        holidayCalendar.AddHolidayRule(new DayOfTheMonthHolidayRule(MonthsOfTheYear.JANUARY,1));
        const aChristmas = new Date(2014,MonthsOfTheYear.JANUARY,25);
        expect(holidayCalendar.IsHoliday(aChristmas)).toBe(false);
    });

    test('7',function(){
        const holidayCalendar = new HolidayCalendar();
        holidayCalendar.AddHolidayRule(new DayOfTheMonthHolidayRule(MonthsOfTheYear.JANUARY,1));
        holidayCalendar.AddHolidayRule(new DayOfTheMonthHolidayRule(MonthsOfTheYear.JANUARY,25));
        const aChristmas = new Date(2014,MonthsOfTheYear.JANUARY,25);
        const aJanuaryFirst = new Date(2014,MonthsOfTheYear.JANUARY,1);
        expect(holidayCalendar.IsHoliday(aJanuaryFirst)).toBe(true);
        expect(holidayCalendar.IsHoliday(aChristmas)).toBe(true);
    });

    test('8',function(){
        const holidayCalendar = new HolidayCalendar();
        const aChristmas = new Date(2014,MonthsOfTheYear.JANUARY,25);
        holidayCalendar.AddHolidayRule(new DateHolidayRule(aChristmas));
        expect(holidayCalendar.IsHoliday(aChristmas)).toBe(true);
    });

    test('9',function(){
        const holidayCalendar = new HolidayCalendar();
        const aDate = new Date(2014,MonthsOfTheYear.JANUARY,12);
        expect(holidayCalendar.IsHoliday(aDate)).toBe(false);
    });

    test('10',function(){
        const holidayCalendar = new HolidayCalendar();
        const aJanuaryFirst = new Date(2014,MonthsOfTheYear.JANUARY,1);
        const aChristmas = new Date(2014,MonthsOfTheYear.JANUARY,25);
        holidayCalendar.AddHolidayRule(new DateHolidayRule(aJanuaryFirst));
        holidayCalendar.AddHolidayRule(new DateHolidayRule(aChristmas));
        expect(holidayCalendar.IsHoliday(aJanuaryFirst)).toBe(true);
        expect(holidayCalendar.IsHoliday(aChristmas)).toBe(true);
    });

    test('11',function(){
        const holidayCalendar = new HolidayCalendar();
        holidayCalendar.AddHolidayRule(new CompoundRangeHolidayRule(
            new Date(1990, MonthsOfTheYear.JANUARY, 1),
            new Date(1999, MonthsOfTheYear.DECEMBER,31),
            new DayOfTheWeekHolidayRule(DaysOfTheWeek.MONDAY)));
        const aMonday = new Date(1998,MonthsOfTheYear.MARCH,2);
        expect(holidayCalendar.IsHoliday(aMonday)).toBe(true);
    });

    test('12',function(){
        const holidayCalendar = new HolidayCalendar();
        holidayCalendar.AddHolidayRule(new CompoundRangeHolidayRule(
            new Date(1990, MonthsOfTheYear.JANUARY, 1),
            new Date(1999, MonthsOfTheYear.DECEMBER,31),
            new DayOfTheWeekHolidayRule(DaysOfTheWeek.MONDAY)));
        const aMonday = new Date(1989,MonthsOfTheYear.DECEMBER,28);
        expect(holidayCalendar.IsHoliday(aMonday)).toBe(false);
    });

    test('13',function(){
        const holidayCalendar = new HolidayCalendar();
        holidayCalendar.AddHolidayRule(new CompoundRangeHolidayRule(
            new Date(1990, MonthsOfTheYear.JANUARY, 1),
            new Date(1999, MonthsOfTheYear.DECEMBER,31),
            new DayOfTheWeekHolidayRule(DaysOfTheWeek.MONDAY)));
        const aMonday = new Date(2000,MonthsOfTheYear.DECEMBER,3);
        expect(holidayCalendar.IsHoliday(aMonday)).toBe(false);
    });

    test('14',function(){
        const holidayCalendar = new HolidayCalendar();
        holidayCalendar.AddHolidayRule(new CompoundRangeHolidayRule(
            new Date(1990, MonthsOfTheYear.JANUARY, 1),
            new Date(1999, MonthsOfTheYear.DECEMBER,31),
            new DayOfTheWeekHolidayRule(DaysOfTheWeek.MONDAY)));
        holidayCalendar.AddHolidayRule(new CompoundRangeHolidayRule(
            new Date(2016, MonthsOfTheYear.JANUARY, 1),
            new Date(2116, MonthsOfTheYear.DECEMBER,31),
            new DayOfTheMonthHolidayRule(MonthsOfTheYear.JUNE,17 )));
        const aMonday = new Date(1998,MonthsOfTheYear.MARCH,2);
        const aJune17th = new Date(2021,MonthsOfTheYear.JUNE,17);
        expect(holidayCalendar.IsHoliday(aMonday)).toBe(true);
        expect(holidayCalendar.IsHoliday(aJune17th)).toBe(true);
    });
    

});