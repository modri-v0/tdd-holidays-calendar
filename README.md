# TDD Holiday Calendar

Ejemplo de TDD [Charlas en Línea: Esencia y Fundamentos de TDD](https://www.youtube.com/watch?v=4t0ZxUXnwN4) hecho en Javascript.

## Ejemplo: calendario de feriados
Modelar un calendario de feriados al que se le pueda indicar qué días son feriados de la siguiente manera:

- Un día de la semana (ej: domingo)
- Un día de un mes (ej: 25 de diciembre)
- Un día en particular (20/04/2012)

### Nueva funcionalidad

Los feriados son validos en un intervalo de tiempo:

- A partir del 2/8/2002 el 24/3 es feriado
- Del 1/1/1990 al 31/12/1999 fueron feriados los lunes

## NOTAS

Javascript no tiene implementado interfaces como si tiene C#, por lo que para definir la interfaz se definió una clase padre y se la extendió.

## TODOs

- Ponerle nombre a los tests
- Separar las clases de los tests
- Tal vez separar los tests por clase
- Mejorar historial de commits (comentarios del commit)
- Armar listado de commits en README.md
- Ejercitar tests con @pmoo/testy
- Revisar comparación de fechas
